# shellcheck shell=sh

load_env() {
	_source_scripts 'shell_functions' 'sh'
	_source_scripts 'shell_login'     'sh'
}

# usage: $1 - hook function
_source_install_hooks() {
	                  _source_scripts "install_$1"   'sh'
	[ -n "$BASH" ] && _source_scripts "install_$1" 'bash'
}

# usage:
#	$1 - optional, sort files in descending order ['desc']
#	$2 - script type [filename prefix]
#	$3 - script extension [filename suffix]
#	$4 - optional, subdirectory of $PEARL_PKGDIR. Include leading '/'.
_source_scripts() {
	if [ "$1" = 'desc' ]; then
		desc='-r'
		shift
	else
		unset desc
	fi

	for file in $(find "${PEARL_PKGDIR}${3}" \
		-mindepth 1 -maxdepth 2          \
		-type f -name "${1}*.${2}"       \
		| LC_ALL='C' sort $desc);
	do
		# shellcheck source=/dev/null
		. "$file"
	done
}

post_install_hooks() { load_env; _source_install_hooks 'post_install'; }
pre_update_hooks()   { load_env; _source_install_hooks 'pre_update';   }
post_update_hooks()  { load_env; _source_install_hooks 'post_update';  }
pre_remove_hooks()   { load_env; _source_install_hooks 'pre_remove';   }
