# shellcheck shell=sh

command_exists 'vim' || return # if vim is not installed

# https://tlvince.com/vim-respect-xdg
export VIMDOTDIR="$PEARL_PKGDIR/vim"
mkdir -p "$VIMDOTDIR"
# shellcheck disable=SC2016
export VIMINIT='let $MYVIMRC = "$VIMDOTDIR/vimrc" | source $MYVIMRC'

# disable CTRL-S as SIGSTOP in terminal to allow using CTRL-S to save stty -ixon

# vim: ft=sh
