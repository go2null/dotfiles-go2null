if has('win32') || has('win64')
	" do not load mswin.vim; want same behavior on all OSs.
	let g:skip_loading_mswin = 1

	" stop if in Cygwin or MSYS2
	let s:uname = system('uname -s')
	if !v:shell_error | finish | endif

	if !exists('$HOME') || !isdirectory($HOME)
		let $HOME = $USERPROFILE
	endif

	" setup PortableApps
	let s:file = expand('<sfile>:p:h') . '/mswin_portableapps.vim'
	if filereadable(s:file)
		source s:file
	endif
endif
