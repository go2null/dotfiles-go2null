augroup vimrc_reload_on_save
	autocmd!
	" reload & dump new settings
	" Reload .vimrc when it's edited
	"    https://stackoverflow.com/questions/2400264/is-it-possible-to-apply-vim-configurations-without-restarting/2403926#2403926
	" dump new settings
	"    http://vim.wikia.com/wiki/Open_vimrc_file
	autocmd BufWritePost $MYVIMRC,*vimrc
		\   source $MYVIMRC
		\ | if has('gui_running')
		\ |    for server in split(serverlist())
		\ |      call remote_send(server, '<Esc>:source $MYVIMRC<CR>')
		\ |      if filereadable($MYGVIMRC)
		\ |        call remote_send(server, '<Esc>:source $MYGVIMRC<CR>')
		\ |      endif
		\ |    endfor
		\ | endif
		\ | mkvimrc! $XDG_CACHE_HOME/vim/vimrc_generated
augroup END
