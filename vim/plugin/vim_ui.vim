"# Mouse
set mouse=a

"# Unicode
" https://stackoverflow.com/questions/5477565/how-to-setup-vim-properly-for-editing-in-utf-8/5795441#5795441"
if has('multi_byte')
	" set keyboard encoding and terminal encoding for for non-gui terms
	if &termencoding == ''
		let &termencoding = &encoding
	endif
	set encoding=utf-8           " internal Vim charset defaults to latin1
	setglobal fileencoding=utf-8 " default for new files

	" Displaying text - Whitespace - List
	scriptencoding utf-8 " required by vimlint
	set listchars=tab:\+\ ,trail:·,extends:»,precedes:«,nbsp:×,eol:¬
endif

"# Bells and whistles
set visualbell  |" disable audible bell/beep
set t_vb=       |" and now turn off visualbell

"# Views
set viewoptions-=cursor viewoptions-=folds viewoptions-=options
set viewoptions+=cursor,folds

"# Layout - Line marker
set colorcolumn=80 " Line marker

"# Layout - Margin
set number relativenumber                 " required for when first start vim
augroup auto_set_line_numbering | autocmd!
	autocmd WinEnter,FocusGained,InsertLeave * :setlocal number relativenumber
	autocmd WinLeave,FocusLost,InsertEnter   * :setlocal number norelativenumber
augroup END

"# Displaying text - Whitespace
set list " display tabs and EOL whitespace
if has('multi_byte')
	scriptencoding utf-8 " required by vimlint
	set listchars=tab:\+\ ,trail:·,extends:»,precedes:«,nbsp:×,eol:¬
endif

"# Mappings - Search
nnoremap <Leader><esc> :noh<return><esc> |" clear search highlighting

"# Mappings - Window creation
nnoremap <Leader><C-w><up>    :topleft   new<CR>
nnoremap <Leader><C-w>k       :topleft   new<CR>
nnoremap <Leader><C-w><left>  :topleft  vnew<CR>
nnoremap <Leader><C-w>h       :topleft  vnew<CR>
nnoremap <Leader><C-w><down>  :botright  new<CR>
nnoremap <Leader><C-w>j       :botright  new<CR>
nnoremap <Leader><C-w><right> :botright vnew<CR>
nnoremap <Leader><C-w>l       :botright vnew<CR>

"# Mappings - Window sizing
"  :help ctrl-w Default keybindings
nnoremap <Leader><C-w>\   :vertical resize 85<CR>
"  85 = 2 (scrollbar) + 3 (margin) + 80 (colorcolumn, line marker)
