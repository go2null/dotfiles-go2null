"# Notes

"## vim help - *map-table*

" " | -------\ Mode  | Norm | Ins | Cmd | Vis | Sel | Opr | Term | Lang |"
" " + Command \------+------+-----+-----+-----+-----+-----+------+------+"
" " |  [nore|un]map  | yes  |  -  |  -  | yes | yes | yes |  -   |  -   |"
" " |  [nore|un]map! |  -   | yes | yes |  -  |  -  |  -  |  -   |  -   |"
" " | c[nore|un]map  |  -   |  -  | yes |  -  |  -  |  -  |  -   |  -   |"
" " | i[nore|un]map  |  -   | yes |  -  |  -  |  -  |  -  |  -   |  -   |"
" " | l[nore|un]map  |  -   | yes | yes |  -  |  -  |  -  |  -   | yes  |"
" " | n[nore|un]map  | yes  |  -  |  -  |  -  |  -  |  -  |  -   |  -   |"
" " | o[nore|un]map  |  -   |  -  |  -  |  -  |  -  | yes |  -   |  -   |"
" " | s[nore|un]map  |  -   |  -  |  -  |  -  | yes |  -  |  -   |  -   |"
" " | t[nore|un]map  |  -   |  -  |  -  |  -  |  -  |  -  | yes  |  -   |"
" " | v[nore|un]map  |  -   |  -  |  -  | yes | yes |  -  |  -   |  -   |"
" " | x[nore|un]map  |  -   |  -  |  -  | yes |  -  |  -  |  -   |  -   |"

"## Notes - Navigation
" " :jumps   " Jumps   " list Jumps
" " #CTRL-O  " Jumps   " #th last visited location
" " #CTRL-I  " Jumps   " #th next visited location
" " :changes " Changes " list changes
" " #g,      " Changes " #th oldest change
" " #g;      " Changes " #th newest change

" setup environment - platforms
source <sfile>:p:h/mswin.vim
" setup environment - shell and vim paths
source <sfile>:p:h/functions.vim
source <sfile>:p:h/paths.vim " requires: functions.vim
" add plugins to `runtimepath`
source <sfile>:p:h/plugins.vim
" now ensure Vim continues sourcing default dirs in `runtimepath`
filetype plugin indent on
" ensure `foldexpr` modeline works
set modeline
set modelineexpr

"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=11
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='
