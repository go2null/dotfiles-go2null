"# User functions

"## Command aliases
command! -nargs=1 -complete=file     Rename  call s:FileRename(<q-args>)
command! -nargs=+ -complete=shellcmd Shell   call s:ShellRun(<q-args>)
command! -nargs=? -complete=dir DumpMappings call s:DumpKeyMappings(<q-args>)

command! -nargs=1 -complete=dir      Mkdir   call FileMkdir(<q-args>)

"## Rename: FileRename
" https://stackoverflow.com/questions/1205286/renaming-the-current-file-in-vim/23370094#23370094
function! s:FileRename(new_path)
	execute 'saveas ' . a:new_path
	silent call delete(expand('#:p'))
	bdelete #
endfunction

"## Shell: ShellRun
" External command output window
" http://vim.wikia.com/wiki/Display_output_of_shell_commands_in_new_window
function! s:ShellRun(command_line)
	" get command
	let expanded_command_line = join(map(split(a:command_line), 'expand(v:val)'))

	" store current window to return to after
	let original_window_number = winnr()

	" store filetype to enable syntax highlighting in scratch buffer
	let file_type = &filetype

	" create/goto scratch window
	call s:GotoScratchWindow('Command_Output', 'botright vnew')

	" save end of buffer; 2 because `zt` returns 1 line above specified line
	let line_num = line('$') + 3

	" print header
	call append(line('$'), 'You entered: ' . a:command_line)
	call append(line('$'), 'Expanded to: ' . expanded_command_line)
	let separator = substitute(getline(line('$')), '.', '=', 'g')
	call append(line('$') - 2, separator)
	call append(line('$'), separator)

	" run command and read input into scratch buffer
	silent execute line('$') . 'read !'. expanded_command_line
	call append(line('.'), '')

	" return to original end of buffer
	execute 'normal ' . line_num . 'zt'

	" enable syntax highlighting, using same filetype as caller buffer
	silent execute 'setlocal filetype=' . file_type

	" return to original window
	execute original_window_number . "wincmd w"
endfunction

function! s:DumpKeyMappings(...)
	let path = a:0 > 0 && a:1 != '' ? a:1 : $XDG_CACHE_HOME . '/vim/key_mappings.txt'
	let path = fnameescape(path)

	execute 'redir! > ' . path
	silent verbose nmap
	silent verbose omap
	silent verbose vmap
	silent verbose map!
	redir END

	silent execute 'edit ' . path
	silent execute '%g/^$/d'
	silent execute '%s/^\(.*\)\n\tLast set from \(.*\)$/\2\t\1/'
	silent execute '%s/^\(.\) /\t\1/'
	silent write

	"TODO: currently manually doing
	"  ggVG - select all
	"  :Tabularize /\t
endfunction

"# Script functions

"## DirNew: FileMkDir
function! FileMkdir(path)
	if !isdirectory(a:path)
		call mkdir(a:path, 'p')
	endif
endfunction

"## Goto or Create a new Scratch Window
function! s:GotoScratchWindow(buffer_name, create_position)
	let window_number = bufwinnr('^' . a:buffer_name . '$')
	if window_number < 0
		silent execute a:create_position . ' ' . a:buffer_name
		setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
	else
		silent execute  window_number . 'wincmd w'
	endif
endfunction

"# UNDER DEVELOPMENT

"## FollowSymlink
" follow symlinked file
" http://inlehmansterms.net/2014/09/04/sane-vim-working-directories/
function! s:FollowSymlink()
	let current_file = expand('%:p')
	" check if file type is a symlink
	if getftype(current_file) == 'link'
		" if it is a symlink resolve to the actual file path
		"   and open the actual file
		let actual_file = resolve(current_file)
		silent! execute 'file ' . actual_file
	end
endfunction

"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=2
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='
