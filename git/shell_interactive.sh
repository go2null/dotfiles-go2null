# shellcheck shell=sh

g() {
	if [ $# -eq 0 ]; then
		git df
		git sts
		git lg
	else
		git "$@"
	fi
}
alias G='g'  # muscle memory

# shellcheck disable=2046 # need the files to be distinct arguements
ge() { $EDITOR -- $(git ls-files); }

grepos() (
	find ~/src/ -mindepth 2 -maxdepth 2 -type d \
	| while IFS= read -r repo; do
			printf '\n\033[1;35m%s\033[0m\n' "$repo"
			cd "$repo" || continue
			git rtl | grep --color=always '/.*.git' \
			git rtu
			git sts
			git lg -1
		done
)

