# don't source if `git` not installed
command_exists 'git' || return

# = BASH COMPLETION = #

# Enable completion for apps that are aliased
complete -F _git g

if [ -r  "$PEARL_PKGDIR/git/git-completion.bash" ]; then
	# shellcheck source-path=SCRIPTDIR/../
	source "$PEARL_PKGDIR/git/git-completion.bash"
fi


# = BASH PROMPT = #

# set prompt in bash and zsh
if [ -r  "$PEARL_PKGDIR/git/git-prompt.sh" ]; then
	# shellcheck source-path=SCRIPTDIR/../
	source "$PEARL_PKGDIR/git/git-prompt.sh"
	# indicate unstaged (*) and staged (+) files present
	export GIT_PS1_SHOWDIRTYSTATE=1
	# indicate stashed ($) files present
	export GIT_PS1_SHOWSTASHSTATE=1
	# indicate untracked (%) files present
	export GIT_PS1_SHOWUNTRACKEDFILES=1
	# indicate HEAD is behind(<), ahead (>), diverged from (<>), same as(=) upstream
	export GIT_PS1_SHOWUPSTREAM='auto verbose'
	# show colored hints
	export GIT_PS1_SHOWCOLORHINTS=154
	# actual PS1 string
	[ "${PS1%git*}" = "$PS1" ] && PS1=${PS1}'$(__git_ps1 "(%s)") '
fi
