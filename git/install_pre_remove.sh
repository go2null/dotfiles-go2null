# shellcheck shell=sh

print_start 'Git'
command_exists 'git' || { print_skip; return; }

print_progress
# use `|| true` to ignore errors as pearl barfs as it uses `set -e`
git config --global --unset-all 'include.path'      "$PEARL_PKGDIR/git/config" || true

print_progress
git config --global --unset-all 'core.excludesFile' "$PEARL_PKGDIR/git/ignore" || true

print_done
