# shellcheck shell=sh

print_start 'Git'
command_exists 'git' || { print_skip; return; }

#
# prefer $XDG_CONFIG_HOME/git/* over $HOME/.git* for local configs

print_progress
[ -e "$XDG_CONFIG_HOME/git/config" ] || mkdir -p "$XDG_CONFIG_HOME/git"

print_progress
if [ ! -e "$XDG_CONFIG_HOME/git/config" ] && [ -e "$HOME/.gitconfig" ]; then
	mv "$HOME/.gitconfig" "$XDG_CONFIG_HOME/git/config"
else
	touch "$XDG_CONFIG_HOME/git/config"
fi

print_progress
if [ ! -e "$XDG_CONFIG_HOME/git/gitk" ] && [ -e "$HOME/.gitk" ]; then
	mv "$HOME/.gitk" "$XDG_CONFIG_HOME/git/gitk"
else
	touch "$XDG_CONFIG_HOME/git/gitk"
fi


# include global files

print_progress
git config --global --unset-all 'include.path'      "$PEARL_PKGDIR/git/config" || true
git config --global --add       'include.path'      "$PEARL_PKGDIR/git/config"

print_progress
git config --global --unset-all 'core.excludesFile' "$PEARL_PKGDIR/git/ignore" || true
git config --global --add       'core.excludesFile' "$PEARL_PKGDIR/git/ignore"

print_progress
# gitk uses last setting, and saves on exit.
cat "$PEARL_PKGDIR/git/solarized-dark.gitk" >> "$XDG_CONFIG_HOME/git/gitk"

print_done
