FROM go2null/pearl

ENV XDG_CONFIG_HOME "$HOME/.config"
RUN mkdir -p "$XDG_CONFIG_HOME/pearl"

RUN echo 'PEARL_PACKAGES["dotfiles-go2null"] = {"url": "https://gitlab.com/go2null/dotfiles-go2null.git", "description": "go2null dotfiles"}' \
	>> "$XDG_CONFIG_HOME/pearl/pearl.conf"

RUN echo 'PEARL_PACKAGES["test"] = {"url": "/mnt"}' >> "$XDG_CONFIG_HOME/pearl/pearl.conf"
