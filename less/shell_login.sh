# shellcheck shell=sh

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support

# less
export LESSHISTFILE="$XDG_CACHE_HOME/lesshst"    # ~/.lesshst
export LESSKEY="$PEARL_PKGDIR/less/lesskey"
