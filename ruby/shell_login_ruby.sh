# shellcheck shell=sh

command_exists 'ruby' || return

# = Bundle = #
#	BUNDLE_CONFIG      file, highest priority than BUNDLE_USER_CONFIG
#	BUNDLE_USER_CACHE  directory
#	BUNDLE_USER_CONFIG file
#	BUNDLE_USER_HOME   directory
#	BUNDLE_USER_PLUGIN directory

while read -r env_var path; do
	export "$env_var"="$path"
done <<-ENVVAR
	BUNDLE_USER_HOME   $XDG_DATA_HOME/bundle
	BUNDLE_USER_CONFIG $XDG_CONFIG_HOME/bundle
	BUNDLE_USER_CACHE  $XDG_CACHE_HOME/bundle
	BUNDLE_USER_PLUGIN $XDG_DATA_HOME/bundle/plugin
ENVVAR
