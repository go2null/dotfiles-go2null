# shellcheck shell=sh

cp "$PEARL_PKGDIR/ruby/pryrc" "$XDG_CONFIG_HOME/pry/"
print_start 'Ruby'
command_exists 'ruby' || { print_skip; return; }

#
# PRY

print_in_progress
cp "$PEARL_PKGDIR/ruby/pryrc" "$XDG_CONFIG_HOME/pry/"


print_done
