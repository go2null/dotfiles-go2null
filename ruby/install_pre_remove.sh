# shellcheck shell=sh

print_start 'Ruby'
command_exists 'ruby' || { print_skip; return; }


# PRY

print_in_progress
rm -i "$XDG_CONFIG_HOME/pry/pryrc"
rmdir "$XDG_CONFIG_HOME/pry"


print_done
