# shellcheck shell=sh

[ -n "$MSYSTEM" ] || return

pman() {
	local action pkg pkgs

	action="$1"
	shift

	# shellcheck disable=SC2048
	for pkg in $*; do
		pkgs="$pkgs $MINGW_PACKAGE_PREFIX-$pkg"
	done

	# shellcheck disable=SC2086
	pacman "$action" $pkgs
}
