# shellcheck shell=sh

print_start 'Helix'
command_exists 'helix' || { print_skip; return; }

# run in subshell for local variables
(
	# Helix has this hardcoded for Windows, whether in MSYS2 or not
	if [ "$OS" = 'Windows_NT' ]; then
		config_home="$APPDATA/helix"
	else
		config_home="$XDG_CONFIG_HOME/helix"
	fi

	print_progress
	if [ ! -d "$config_home" ]; then
		mkdir -p "$config_home"
	fi

	print_progress
	cp 'helix/config.toml' "$config_home/"
)

print_done
