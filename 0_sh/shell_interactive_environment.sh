# shellcheck shell=sh

# = POSIX ENVIRONMENT VARIABLES = #

if command -v gvim  >/dev/null 2>&1; then
	export EDITOR='gvim'
elif command -v vim >/dev/null 2>&1; then
	export EDITOR='vim'
elif command -v vi  >/dev/null 2>&1; then
	export EDITOR='vi'
fi

export VISUAL="$EDITOR"

# http://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html
# keep the last 5000 entries
export HISTSIZE=5000

# Set the PS1 prompt (with colors).
PS1="\[\033[36;1m\]\u@\h:\[\033[32;1m\]\w\$\[\033[0m\] "

export PAGER='less -r'


# = SUDO = #

if command -v sudo >/dev/null; then
	path_add '/usr/local/sbin' '/usr/sbin' '/sbin'
fi


# = TERM = #

# causes background to be highlighted, instead of solid, in gnu-screen.
[ "$TERM" != "screen-256color" ] && export TERM='xterm-256color'
