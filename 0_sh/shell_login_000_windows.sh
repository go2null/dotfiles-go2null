# shellcheck shell=sh

[ "$OS" != 'Windows_NT' ] && return

# Git-for-Windows uses MSYS2 which uses mintty (Cygwin)

# ensure is set
export HOME="${HOME:-$USERPROFILE}"
export USER="${USER:-$USERNAME}"
