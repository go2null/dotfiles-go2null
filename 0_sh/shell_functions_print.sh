# shellcheck shell=sh

print_line()    { printf '%s\n' "$*";  }

print_debug()   { [ -n "$DEBUG" ] && print_line "DEBUG:   $*"; return; }
print_info()    { print_line   "INFO:    $*"; return; }
print_warning() { print_line   "WARNING: $*"; return; }

print_stderr()  { print_line "$*" >&2; }
print_error()   { print_stderr "ERROR:   $*"; return 1; }
print_fatal()   { print_stderr "FATAL:   $*"; exit   1; }
