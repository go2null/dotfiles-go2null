# shellcheck shell=sh

print_start 'Shell'

# == xdg ==

# http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
#	Version 0.8	08th May 2021

# TODO: implemented in: /etc/X11/xinit/xinitrc.xfce,
# but doesn't appear in xfce4-terminal nor uxterm.

# NOTE: XDG_BIN_HOME environment variable is not formally defined.
#		The path is recommnded by XDG, and required by SystemD.
#		Defining here to make it easy to use.
print_progress
while read -r xdg; do
	mkdir -p "$(eval "printf '%s' \$$xdg")"
done <<-XDG
	XDG_BIN_HOME
	XDG_DATA_HOME
	XDG_CONFIG_HOME
	XDG_STATE_HOME
	XDG_CACHE_HOME
	XDG_RUNTIME_DIR
XDG
chown "$USER:$(id -gn)" "$XDG_RUNTIME_DIR"
chmod o=-rwx            "$XDG_RUNTIME_DIR"

## https://wiki.freedesktop.org/www/Software/xdg-user-dirs/
## TODO: implemented in: /etc/X11/xinit/xinitrc.xfce
## but `xdg-user-dirs-update` doesn't exist.


# == systemd ==

# https://www.freedesktop.org/software/systemd/man/file-hierarchy.html#~/.local/bin/
print_progress
# link to ~/bin for convenience and backwards compatibility.
# Cannot create symlinks on Windows without Local Admin
#   or Developer Mode enabled (in Windows Settings)
[ -z "$MSYSTEM" ] \
	&& [ ! -e "$HOME/bin" ] \
	&& ln -s "$XDG_BIN_HOME" "$HOME/bin"

# https://www.freedesktop.org/software/systemd/man/file-hierarchy.html#~/.local/lib/
print_progress
mkdir -p "$HOME/.local/lib"

print_done
