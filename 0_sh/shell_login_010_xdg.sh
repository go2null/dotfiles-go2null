# shellcheck shell=sh

# http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html

while read -r env_var path; do
	export "$env_var"="$path"
done <<-XDG
	XDG_BIN_HOME    $HOME/.local/bin
	XDG_DATA_HOME   $HOME/.local/share
	XDG_CONFIG_HOME $HOME/.config
	XDG_STATE_HOME  $HOME/.local/state
	XDG_CACHE_HOME  $HOME/.cache
	XDG_RUNTIME_DIR /run/user/$(id -u)
XDG

# https://www.freedesktop.org/software/systemd/man/file-hierarchy.html

path_prepend "$XDG_BIN_HOME"
