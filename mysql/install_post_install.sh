# shellcheck shell=sh

print_start 'MySQL'
command_exists 'mysql' || { print_skip; return; }

print_progress
link_to "$PEARL_PKGDIR/mysql/my.cnf" "$HOME/.my.cnf"

print_done
