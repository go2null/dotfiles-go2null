# shellcheck shell=sh

command_exists 'mysql' || return 0

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support
export MYSQL_HISTFILE="$XDG_CACHE/mysql_history" # ~/.mysql_history
