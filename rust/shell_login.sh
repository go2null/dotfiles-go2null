# shellcheck shell=sh

command_exists 'rust' || return

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support
# https://internals.rust-lang.org/t/pre-rfc-split-cargo-home/19747

while read -r env_var path; do
	export "$env_var"="$path"
done <<-ENVVAR
	CARGO_HOME         $XDG_DATA_HOME/cargo
	CARGO_USER_HOME    $XDG_DATA_HOME/cargo
	CARGO_USER_CONFIG  $XDG_CONFIG_HOME/cargo
	CARGO_USER_CACHE   $XDG_DATA_HOME/cargo
	RUSTUP_HOME        $XDG_DATA_HOME/rustup
	RUSTUP_USER_HOME   $XDG_DATA_HOME/rustup
	RUSTUP_USER_CONFIG $XDG_CONFIG_HOME/rustup
	RUSTUP_USER_CACHE  $XDG_DATA_HOME/rustup
ENVVAR
