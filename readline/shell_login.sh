# shellcheck shell=sh

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support

export INPUTRC="$PEARL_PKGDIR/readline/inputrc"        # ~/.inputrc
